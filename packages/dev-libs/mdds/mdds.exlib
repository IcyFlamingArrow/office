# Copyright 2013 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

export_exlib_phases src_prepare

SUMMARY="Collection of multi-dimensional data structures and indexing algorithms"
DESCRIPTION="
mdds provides the following data structures:
Segment tree:      A balanced-binary-tree based data structure efficient for detecting all intervals
                   (or segments) that contain a given point.
Flat segment tree: A variant of segment tree that is designed to store a
                   collection of non-overlapping segments.
Rectangle set:     Stores 2-dimensional rectangles and provides an efficient way to
                   query all rectangles that contain a given point in 2-dimensional space.
Point quad tree:   Stores 2-dimensional points and provides an efficient way to
                   query all points within specified rectangular region.
Multi-type vector: Storage of unspecified number of types in a single logical array such that
                   contiguous elements of identical type are stored in contiguous segment in memory.
Multi-type matrix: A matrix structure that allows storage of four different element types:
                   numeric, string, boolean and empty.
"
DOWNLOADS="https://kohei.us/files/mdds/src/${PNV}.tar.bz2"

LICENCES="MIT"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-libs/boost
    test:
        dev-tcl/expect
        dev-util/dejagnu [[ note = [ runtest ] ]]
        sys-devel/gdb
"

DEFAULT_SRC_CONFIGURE_PARAMS+=(
    --datarootdir=/usr/$(exhost --target)/lib
    --enable-release-tests
    --disable-docs
    --disable-memory-tests
    --disable-werror
    --disable-openmp
)

mdds_src_prepare() {
    # honor flags
    edo sed \
        -e 's/-O2//' \
        -i configure.ac

    autotools_src_prepare
}

